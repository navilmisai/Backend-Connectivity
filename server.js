var express = require('express');
var app = express();  // Variable to get the Req and Res.
var cors = require('cors');
var bodyParser=require('body-parser'); 
var mongoose =require('mongoose');
var jwt = require('jwt-simple');

var port = process.env.PORT || 8080; // set the port
var User = require('./models/user.js');
var posts = [
    {message: 'Hi Parthiban'},
    {greetings: 'How are you'}
]


app.use(cors());

app.use(bodyParser.json());


app.get('/posts', (req,res) =>{
    res.send(posts);  
    
});


//Display Users from Backend

app.get('/users', async(req, res) =>{
    try{
        let users = await User.find({}, '-password -__v');
        res.send(users);
         }
         catch (error){
             console.log(error);
             res.sendStatus(200);
         }
})
app.get('/profile/:id', async(req,res)=> {
    try{
    let user= await User.findById(req.params.id,'-password -__v')
    res.send(user);
    }catch(error){
consolee.log(error);
res.sendStatus(500);
    }
})

app.post('/register', (req,res) =>{
       let userData = req.body;
        console.log(userData);

    let user = new User(userData);
    user.save((err,result) =>{
        if(err){
            console.log('There is trouble adding User Parthi.');
        }else{
            res.sendStatus(200);
        }
    })

});

//Token - End Point

app.post('/login', async(req,res) =>{
    let userData = req.body;
    let user = await User.findOne({email:userData.email});
    if (!user){
        return res.status(401).send({message: 'Email or Password invalid'})
    }
    if(userData.password !=user.password){
        return res.status(401).send({message: 'Password is invalid'})
    }
    let payload = {"id": user.email}
    let token = jwt.encode(payload, '23456')
    res.send(user);
    //res.status(200).send({payload});
});


mongoose.connect('mongodb://Nodeuser:Nodeuser@172.30.141.126:27017/IoT', { useNewUrlParser: true, useUnifiedTopology: true },(err) =>{
//mongoose.connect('mongodb://nodeuser:nodeuser@172.30.90.120:27017/iot', { useNewUrlParser: true, useUnifiedTopology: true },(err) =>{
//mongoose.connect('mongodb+srv://Priyankaa:ramapuram@priyankaa-kea1u.mongodb.net/test?retryWrites=true&w=majority',(err) =>{
if(!err){
   console.log('Connected to Database');
}    else
    console.log("Failed");
});


app.listen(port);